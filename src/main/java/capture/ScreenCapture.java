package capture;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ScreenCapture {

	
	public static void main(String[] args) {
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 0, 0));
		
		JFrame.setDefaultLookAndFeelDecorated(true);
		
		JFrame frame = new JFrame();
		frame.setAlwaysOnTop(true);
		//frame.ssetAl
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Screen Capture");
		frame.setUndecorated(true);
		frame.setBackground(new Color(0, 0, 0, 0));
		frame.getContentPane().add(panel);
		
		frame.addKeyListener(new CaptureListener(panel));
		
		frame.setVisible(true);
	}
}
