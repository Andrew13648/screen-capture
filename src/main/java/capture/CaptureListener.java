package capture;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JPanel;

public class CaptureListener extends KeyAdapter {
		
	private final static String SCREENSHOTS_DIR = "C:\\Screenshots\\";
	private JPanel panel;
		
	public CaptureListener(JPanel panel) {
		this.panel = panel;
	}

	@Override
	public void keyPressed(KeyEvent e) {

		super.keyPressed(e);
		
		if (KeyEvent.VK_SPACE == e.getKeyCode()) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss-SSS");
			try {
				Robot robot = new Robot();
				BufferedImage screenShot = robot.createScreenCapture(new Rectangle(panel.getLocationOnScreen(), panel.getSize()));
				File file = new File(SCREENSHOTS_DIR + sdf.format(new Date()) + ".png");
				ImageIO.write(screenShot, "PNG", file);
				playSound("camera.wav");
			}
			catch (Exception ex) {
				System.err.println(ex);
			}
		}
	}

	private void playSound(String fileName) {
		try {
			Clip clip = AudioSystem.getClip();
			File file = new File(getClass().getClassLoader().getResource(fileName).getFile());
			clip.open(AudioSystem.getAudioInputStream(file));
			clip.start();
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
