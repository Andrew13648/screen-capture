This is a very simple Java application that can be used to take screenshots in a PNG format.  To use, run the `ScreenCapture` class and then press spacebar to take a screenshot.

